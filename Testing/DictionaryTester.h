//
// Created by bshort092 on 4/1/2017.
//

#ifndef HW6_DICTIONARY_DICTIONARYTESTER_H
#define HW6_DICTIONARY_DICTIONARYTESTER_H


class DictionaryTester {
public:
    void testAdd();
    void testGetCount();
    void testGetByKey();
    void testGetByIndex();
    void testRemoveByKey();
    void testRemoveByIndex();
};


#endif //HW6_DICTIONARY_DICTIONARYTESTER_H

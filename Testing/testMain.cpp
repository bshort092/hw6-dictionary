//
// Created by bshort092 on 4/1/2017.
//
#include <iostream>
#include <string>
#include "DictionaryTester.h"
#include "KeyValueTester.h"

using namespace std;

int main() {
    cout << "Execute Test Cases" << endl;

    DictionaryTester dictionaryTester;
    dictionaryTester.testAdd();
    dictionaryTester.testGetCount();
    dictionaryTester.testGetByKey();
    dictionaryTester.testGetByIndex();
    dictionaryTester.testRemoveByKey();
    dictionaryTester.testRemoveByIndex();

    KeyValueTester keyValueTester;
    keyValueTester.testGettersAndSetters();

    return 0;
}
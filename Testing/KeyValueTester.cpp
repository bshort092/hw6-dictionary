//
// Created by bshort092 on 4/1/2017.
//

#include "KeyValueTester.h"
#include "../KeyValue.h"
#include <iostream>
using namespace std;

void KeyValueTester::testGettersAndSetters()
{
    cout << "TestSuite KeyValueTester::testGettersAndSetters" << endl;
    KeyValue<string,string> keyValue;
    keyValue.setKey("hair color");
    keyValue.setValue("red");

    if(keyValue.getKey() != "hair color")
    {
        cout << "Unexpected key " << keyValue.getKey() << "expected 'hair color'" << endl;
    }

    if(keyValue.getValue() != "red")
    {
        cout << "Unexpected value " << keyValue.getKey() << "expected 'red'" << endl;
    }
}
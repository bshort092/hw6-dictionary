//
// Created by bshort092 on 4/1/2017.
//

#include "DictionaryTester.h"
#include "../Dictionary.h"
#include <iostream>

void DictionaryTester::testAdd()
{
    cout << "TestSuite DictionaryTester::testAdd" << endl;

    Dictionary<string, string> myDictionary;
    myDictionary.add("blah", "blah");
    myDictionary.add("Keisha", "KJ");
    myDictionary.add("Favorite Class", "CS 1410");

    if(myDictionary.getCount() != 3)
    {
        cout << "Unexpected count " << myDictionary.getCount() << "expected 3";
    }

    try
    {
        myDictionary.add("Favorite Class", "CS 1410");
        cout << "ERROR Trying to add a duplicate KeyValue" << endl;
    }
    catch(invalid_argument){}
}
void DictionaryTester::testGetCount()
{
    cout << "TestSuite DictionaryTester::testGetCount" << endl;
    Dictionary<string, string> myDictionary;
    myDictionary.add("blah", "blah");
    myDictionary.add("Keisha", "KJ");
    myDictionary.add("Favorite Class", "CS 1410");

    if(myDictionary.getCount() != 3)
    {
        cout << "Unexpected count " << myDictionary.getCount() << "expected 3";
    }

    try
    {
        myDictionary.getByKey("Brandon");
        cout << "ERROR KeyValue does not exist" << endl;
    }
    catch(invalid_argument){}
}
void DictionaryTester::testGetByKey()
{
    cout << "TestSuite DictionaryTester::testGetByKey" << endl;

    Dictionary<string, string> myDictionary;
    myDictionary.add("blah", "blah");
    myDictionary.add("Keisha", "KJ");
    myDictionary.add("Favorite Class", "CS 1410");

    KeyValue<string, string> studyGroup = myDictionary.getByKey("Keisha");

    if(studyGroup.getValue() != "KJ")
    {
        cout << "Unexpected KeyValue " << endl;
    }
}
void DictionaryTester::testGetByIndex()
{
    cout << "TestSuite DictionaryTester::testGetByIndex" << endl;

    Dictionary<string, string> myDictionary;
    myDictionary.add("blah", "blah");
    myDictionary.add("Keisha", "KJ");
    myDictionary.add("Favorite Class", "CS 1410");

    KeyValue<string, string> studyGroup = myDictionary.getByIndex(1);

    if(studyGroup.getValue() != "KJ")
    {
        cout << "Unexpected KeyValue " << endl;
    }
}
void DictionaryTester::testRemoveByKey()
{
    cout << "TestSuite DictionaryTester::testRemoveByKey" << endl;

    Dictionary<string, string> myDictionary;
    myDictionary.add("blah", "blah");

    myDictionary.removeByKey("blah");

    int count = myDictionary.getCount();

    if( count == 1)
    {
        cout << "Unexpected count " << count << " expected 0";
    }

    try
    {
       myDictionary.removeByKey("blah");
        cout << "ERROR. Trying to remove an object that has already been removed" << endl;
    }
    catch(invalid_argument) {}

}
void DictionaryTester::testRemoveByIndex()
{
    cout << "TestSuite DictionaryTester::testRemoveByIndex" << endl;

    Dictionary<string, string> myDictionary;
    myDictionary.add("blah", "blah");

    myDictionary.removeByIndex(0);

    int count = myDictionary.getCount();

    if( count == 1)
    {
        cout << "Unexpected count " << count << " expected 0";
    }

    try
    {
        myDictionary.removeByIndex(0);
        cout << "ERROR. Trying to remove an object that has already been removed" << endl;
    }
    catch(invalid_argument) {}
}
#include <iostream>
#include <string>
#include "Dictionary.h"


int main() {

    Dictionary<string, string> myDictionary1;
    Dictionary<string, int> myDictionary2;

    cout << "Adding an KeyValue pair to myDictionary1" << endl;
    myDictionary1.add("hair color", "red");

    cout << "Adding two KeyValue pairs to myIntDictionary" << endl;
    myDictionary2.add("Age", 30);
    myDictionary2.add("Seven", 7);

    cout << "count of first dictionary is: " << myDictionary1.getCount() << endl;
    cout << "count of second dictionary is: " << myDictionary2.getCount() << endl;


    cout << "Removing a KeyValue pair from myDictionary1" << endl;
    myDictionary1.removeByKey("hair color");

    cout << "Removing both KeyValue pairs from myDictionary2" << endl;
    myDictionary2.removeByIndex(1);
    myDictionary2.removeByKey("Age");

    cout << "count of first dictionary is: " << myDictionary1.getCount() << endl;
    cout << "count of second dictionary is: " << myDictionary2.getCount() << endl;

    return 0;
}
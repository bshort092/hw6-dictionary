//
// Created by bshort092 on 4/1/2017.
//

#ifndef HW6_DICTIONARY_DICTIONARY_H
#define HW6_DICTIONARY_DICTIONARY_H

#include "KeyValue.h"
#include <vector>
#include <iostream>
using namespace std;

template <typename K, typename V>
class Dictionary
{
public:
    Dictionary<K, V>();
    Dictionary<K, V>(unsigned int);
    Dictionary<K,V> (const Dictionary& obj); //Copy Constructor
    ~Dictionary();

    void add(const K& key, const V& value);

    int getCount() const {return m_count;}

    const KeyValue<K,V>& getByKey( const K& key) const;
    const KeyValue<K,V> getByIndex(int index) const;

    void removeByKey(const K& key);
    void removeByIndex(int index);


private:
    KeyValue<K, V>*  m_elements=nullptr;
    unsigned int     m_allocated=0;
    unsigned int     m_count=0;

    void grow();
};
unsigned int DEFAULT_CONTAINER_SIZE = 10;

template <typename K, typename V>
Dictionary<K,V>::Dictionary()
{
    m_allocated = DEFAULT_CONTAINER_SIZE;
    m_elements = new KeyValue<K,V>[m_allocated];
    m_count=0;
}
/*
 * Copy Constructor
 */
template <typename K, typename V>
Dictionary<K,V>::Dictionary(const Dictionary& obj)
{
    m_allocated = obj.m_allocated;
    m_elements = obj.m_elements;
    m_count = obj.m_count;

    for (int i = 0; i < m_allocated; i++)
    {
        m_elements[i] = obj.m_elements[i];
    }
}
/*
 * Size Constructor
 */
template <typename K, typename V>
Dictionary<K,V>::Dictionary(unsigned int size)
{
    m_count = 0;
    m_allocated = size;
    m_elements = new KeyValue<K,V>[m_allocated];
}
/*
 * Destructor
 */
template <typename K, typename V>
Dictionary<K,V>::~Dictionary()
{
    if (m_elements!=nullptr)
    {
        delete [] m_elements;
        m_elements = nullptr;
    }
}
/*
 * Remove by index
 */
template <typename K, typename V>
void Dictionary<K,V>::removeByIndex(int index)
{
    if (index < m_count)
    {
        //delete m_elements[index];

        for (int i=index; i<m_count-1; i++)
        {
            m_elements[index] = m_elements[index+1];
        }
        m_count--;
    }
    else
    {
        throw invalid_argument("Invalid Index");
    }
}
/*
 * Remove KeyValue by key
 */
template <typename K, typename V>
void Dictionary<K,V>::removeByKey(const K& key)
{
    int indexFound = -1;
    for (int i = 0; i < m_count && indexFound == -1; ++i)
    {
        if (key == m_elements[i].getKey())
        {
            indexFound = i;
        }
    }
    if(indexFound >= 0)
    {
        removeByIndex(indexFound);
    }
    else
    {
      throw invalid_argument("Invalid Key");
    }
}
/*
 * Add
 */

template <typename K, typename V>
void Dictionary<K,V>::add(const K& key, const V& value)
{
    KeyValue<K,V> element;
    element.setKey(key);
    element.setValue(value);
    if (m_allocated == 0)
    {
            m_allocated = DEFAULT_CONTAINER_SIZE;
            m_elements = new KeyValue<K,V>[m_allocated];
    }

    if (m_count == m_allocated)
        grow();
    //Checking to see if the KeyValue already exists in the array
    for (int i = 0; i <m_count; ++i)
    {
        if(key == m_elements[i].getKey() || value == m_elements[i].getValue())
        {
            throw invalid_argument("Duplicate key found");
        }
    }
    m_elements[m_count++] = element;
}
/*
 * Grow
 */
template <typename K, typename V>
void Dictionary<K,V>::grow()
{
    m_allocated *= 2;
    KeyValue<K,V>* newElements = new KeyValue<K,V>[m_allocated];

    for (int i=0; i<m_allocated; i++)
    {
        newElements[i] = m_elements[i];
    }
    delete [] m_elements;

    m_elements = newElements;
}
/*
 * Get KeyValue by key
 */
template <typename K, typename V>
const KeyValue<K,V>& Dictionary<K, V>::getByKey(const K& key) const
{
    for (int i = 0; i < m_count; i++)
    {
        if (key == m_elements[i].getKey())
        {
            return m_elements[i];
        }
    }

    throw invalid_argument("Key not found");
}
/*
 * Get KeyValue by index
 */
template <typename K, typename V>
const KeyValue<K,V> Dictionary<K,V>::getByIndex(const int index) const
{
    KeyValue<K,V> result;
    if (m_count>0 && index < m_count)
    {
        result = m_elements[index];
    }
    return result;
}
#endif //HW6_DICTIONARY_DICTIONARY_H
